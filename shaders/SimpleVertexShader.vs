#version 450 core
layout(location = 0) in vec4 position;
layout(location = 1) in vec4 center;
layout (location = 0) uniform mat4 view;
layout (location = 1) uniform mat4 projection;

void main(){
	gl_Position = projection * view * (center + vec4(position.xyz, 1.f));
}
