#version 450 core

layout(local_size_x = 64) in;

layout(std140, binding=1) restrict buffer Cen{
    vec4 particleCenters[];
};

layout(std140, binding=2) restrict buffer Vel{
    vec3 velocity[];
};

layout(location=0) uniform float dt;
layout(location=1) uniform vec4 center;
//layout(location=1) uniform float r;
//

struct AABB {
    vec3 min;
    vec3 max;
};

float pointDistToAABB(AABB b, vec4 point){
    float dist = 0.0;

    if (point.x < b.min.x){
        dist += pow(b.min.x - point.x, 2);
    }

    if (point.x > b.max.x){
        dist += pow(point.x - b.max.x, 2);
    }

    if (point.y < b.min.y){
        dist += pow(b.min.y - point.y, 2);
    }

    if (point.y > b.max.y){
        dist += pow(point.y - b.max.y, 2);
    }

    if (point.z < b.min.z){
        dist += pow(b.min.z - point.z, 2);
    }

    if (point.z > b.max.z){
        dist += pow(point.z - b.max.z, 2);
    }

    return dist;
}

bool isSphereCollision(AABB b, vec4 center, float r){
    float sqDist = pointDistToAABB(b, center);
    if (sqDist >= r*r){
        return false;
    }
    return true;
}

bool isAABBCollision(AABB a, AABB b){
    if (a.max.x < b.min.x || a.min.x > b.max.x) return false;
    if (a.max.y < b.min.y || a.min.y > b.max.y) return false;
    if (a.max.z < b.min.z || a.min.z > b.max.z) return false;
    return true;
}


void main(){
    const uint idx = gl_GlobalInvocationID.x;
    vec4 cen = particleCenters[idx];

    AABB box = AABB(vec3(-1.0,-2.0,-1.0), vec3(1.0,-1.5,1.0));
    AABB particleBox = AABB(vec3(cen.x-0.01f, cen.y-0.01f, cen.z ), vec3(cen.x+0.01f, cen.y+0.01f, cen.z));

    if(!isSphereCollision(box, center, 0.3)){
        cen.xyz += velocity[idx].xyz * dt;
    }
    else{
        if(!isAABBCollision(particleBox, box)){
            cen.y += -1.8 * dt;
        }
    }
    particleCenters[idx] = cen;
}

