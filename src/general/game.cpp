#include "game.h"
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <limits.h>
#include <unistd.h>
#include <vector>
#include "shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <random>
#include "particle.h"
#include "pathconfig.h"

Game::Game()
{
    glewExperimental = true;
    if (!glfwInit()){
        std::cout << "Failed to initialize GLFW";
        return;
    }
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}
void Game::runGame(){
    static std::vector<glm::vec4> centerData;
    static std::vector<Particle> particleData;
    static std::vector<glm::vec4> vel;

    //    double r = 1.0;
    //    for (double i=0; i<1; i+=0.0007){
    //        vertexData.emplace_back(glm::vec4(r *  std::sin(i), r * std::cos(i), 0.0f, 1.0f));
    //    }

    const int CNT = 100000;
    std::random_device generator;
    std::uniform_real_distribution<double> phiDistribution(0,360); //fi
    std::uniform_real_distribution<double> thetaDistribution(0,180); //theta
    std::uniform_real_distribution<double> xDistribution(-10,10);
    std::uniform_real_distribution<double> yDistribution(0.8,2);
    std::uniform_real_distribution<double> zDistribution(0,0.3);
    std::uniform_real_distribution<double> velDistribution(-1, -0.2);

    glm::vec4 center(0.0f,1.0f,0.0f,1.0f);

    std::vector<glm::vec4> positions = {
        glm::vec4(-0.01f, -0.01f, 0.0f,1.0f),
        glm::vec4(0.01f, -0.01f, 0.0f, 1.0f),
        glm::vec4(-0.01f, 0.01f, 0.0f,1.0f),
        glm::vec4(0.01f, 0.01f, 0.0f,1.0f)
    };

    centerData.reserve(CNT);
    vel.reserve(CNT);
    particleData.reserve(CNT);
    for(double i=0; i<CNT; i++){
        double x = xDistribution(generator); //fi
        double y = yDistribution(generator); //theta
        double z = zDistribution(generator);
        double speed = velDistribution(generator);

        particleData.emplace_back(Particle(glm::vec4(x, y, 0.0f, 1.0f), glm::vec3(0.0, speed, 0.0), 0.0));
        centerData.emplace_back((glm::vec4(x, y, z, 1.0f)));

        //code fore generating sphere - x is fi, y is theta
        //centerData.emplace_back((glm::vec4(z * std::cos(x) * std::sin(y),
        //                                   center.y + z * std::sin(y) * std::sin(x),
        //                                   z * std::cos(y), 1.0f)));
        vel.emplace_back(glm::vec4(0.0, -1.0, 0.0, 0.0));
    }

    const std::pair<int, int> winSize = std::make_pair<int,int>(1024,768);
    auto window = glfwCreateWindow(winSize.first , winSize.second, "Particles", NULL, NULL);
    glfwMakeContextCurrent(window);
    glewExperimental = true;
    if(glewInit() != GLEW_OK){
        std::cout << "Failed to initialize GLEW";
        return;
    }

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Shader shader;
    const std::string simple_vshader_path {shaders_folder + "/SimpleVertexShader.vs"};
    const std::string simple_fshader_path {shaders_folder + "/SimpleFragmentShader.fs"};
    // shader.loadGeneralShaders(simple_vshader_path.c_str(), simple_fshader_path.c_str());

    // Shader computeShader;
    const std::string simple_cshader_path {shaders_folder + "/SimpleComputeShader.cs"};
    // computeShader.loadComputeShader(simple_cshader_path.c_str());

    std::vector<ShaderInfo> simple_shaders = {
            {GL_VERTEX_SHADER, simple_vshader_path},
            {GL_FRAGMENT_SHADER, simple_fshader_path}
        };

    Shader vf_shader(simple_shaders);

    std::vector<ShaderInfo> compute_shaders = {
            {GL_COMPUTE_SHADER, simple_cshader_path}
        };
    Shader c_shader(compute_shaders);

    GLuint vertexBuffer;
    glCreateBuffers(1, &vertexBuffer);
    glNamedBufferStorage(vertexBuffer, positions.size() * sizeof(glm::vec4), &positions.front(), GL_DYNAMIC_STORAGE_BIT);
    glBindBufferBase(GL_ARRAY_BUFFER, 0, vertexBuffer);

    GLuint centerBuffer;
    glCreateBuffers(1, &centerBuffer);
    glNamedBufferStorage(centerBuffer, centerData.size() * sizeof(glm::vec4), &centerData.front(), GL_DYNAMIC_STORAGE_BIT);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, centerBuffer);

    GLuint velBuffer;
    glCreateBuffers(1, &velBuffer);
    glNamedBufferStorage(velBuffer, vel.size() * sizeof (glm::vec4), &vel.front(), GL_DYNAMIC_STORAGE_BIT);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, velBuffer);

    glm::mat4 projection = glm::perspective(glm::radians(45.0f), float(winSize.first) / float(winSize.second), 0.0f, 100.0f);
    glm::mat4 view = glm::lookAt(
                glm::vec3(0,0,5),
                glm::vec3(0,0,0),
                glm::vec3(0,1,0));

    double lastTime = 0.0;
    double currentTime;
    double dt;

    while( glfwWindowShouldClose(window) == 0){
        currentTime = glfwGetTime();
        dt = currentTime - lastTime;

//        computeShader.use();
//        computeShader.setFloat("dt", float(dt));
//        computeShader.setVec4("center", center);
        c_shader.use();
        c_shader.setFloat("dt", float(dt));
        c_shader.setVec4("center", center);
        glDispatchCompute(CNT >> 5,1,1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

//        shader.use();
//        shader.setMatrix("view", view);
//        shader.setMatrix("projection", projection);
        vf_shader.use();
        vf_shader.setMat4("view", view);
        vf_shader.setMat4("projection", projection);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.0f, 0.0f, 0.2f, 0.0f);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4),(void*) 0);

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, centerBuffer);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4),(void*) 0);

        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, velBuffer);
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec3),(void*) 0);

        glVertexAttribDivisor(0,0);
        glVertexAttribDivisor(1,1);

        glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, GLsizei(positions.size()), CNT);
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glfwSwapBuffers(window);
        glfwPollEvents();
        lastTime = currentTime;

        if (center.y > -1.5){
            center.y += -1.0f * dt;
        }
    }
    return;
}
