#ifndef PARTICLE_H
#define PARTICLE_H

#include <glm/glm.hpp>
#include "shader.h"

struct Particle{

    Particle() = default;
    Particle (glm::vec4 position, glm::vec3 velocity, float life) :
        pos(position), vel(velocity), lifeSpan(life){}

    glm::vec4 pos;
    glm::vec3 vel;
    glm::vec3 color;
    float lifeSpan;
};

#endif

