#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

//#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include "shader.h"
// utility function for checking shader compilation/linking errors.
// ------------------------------------------------------------------------
void checkCompileErrors(GLuint shader, std::string type);

Shader::Shader(const std::vector<ShaderInfo>& shaders)
{
    GLuint ProgramID = glCreateProgram();
    GLint Result = GL_FALSE;
    int InfoLogLength;

    std::vector<GLuint> shader_ids;
    for (const auto& s : shaders)
    {
        GLuint id = glCreateShader(s.type);
        shader_ids.push_back(id);

        std::string shaderCode;
        std::ifstream shaderStream(s.file_path, std::ios::in);
        if (shaderStream.is_open())
        {
            std::stringstream sstr;
            sstr << shaderStream.rdbuf();
            shaderCode = sstr.str();
            shaderStream.close();
        }
        else
        {
            printf("Impossible to open %s. Are you in the right directory ? "
                   "Don't forget to read the FAQ !\n",
                s.file_path.c_str());
            getchar();
            return; // ok, ovo je odvratno raditi u ctoru, ali ne da mi se misliti
        }

        // Compile Shader
        printf("Compiling shader : %s\n", s.file_path.c_str());
        char const* sourcePointer = shaderCode.c_str();
        glShaderSource(id, 1, &sourcePointer, nullptr);
        glCompileShader(id);

        // Check Vertex Shader
        glGetShaderiv(id, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 0)
        {
            std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
            glGetShaderInfoLog(
                id, InfoLogLength, nullptr, &VertexShaderErrorMessage[0]);
            printf("%s\n", &VertexShaderErrorMessage[0]);
        }

        glAttachShader(ProgramID, id);
    }

    glLinkProgram(ProgramID);
    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0)
    {
        std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
        glGetProgramInfoLog(
            ProgramID, InfoLogLength, nullptr, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }

    for (const auto& id : shader_ids)
    {
        glDetachShader(ProgramID, id);
        glDeleteShader(id);
    }
    ID = ProgramID;
}

void checkCompileErrors(GLuint shader, std::string type)
{
    GLint success;
    GLchar infoLog[1024];
    if (type != "PROGRAM")
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout
                << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n"
                << infoLog
                << "\n -- --------------------------------------------------- "
                   "-- "
                << std::endl;
        }
    }
    else
    {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(shader, 1024, nullptr, infoLog);
            std::cout
                << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n"
                << infoLog
                << "\n -- --------------------------------------------------- "
                   "-- "
                << std::endl;
        }
    }
}

void Shader::use() const { glUseProgram(ID); }

// ------------------------------------------------------------------------
void Shader::setBool(const std::string& name, bool value) const
{
    glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
}

// ------------------------------------------------------------------------
void Shader::setInt(const std::string& name, int value) const
{
    glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}

// ------------------------------------------------------------------------
void Shader::setFloat(const std::string& name, float value) const
{
    glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

// ------------------------------------------------------------------------
void Shader::setVec2(const std::string& name, const glm::vec2& value) const
{
    glUniform2fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

// ------------------------------------------------------------------------
void Shader::setVec2(const std::string& name, float x, float y) const
{
    glUniform2f(glGetUniformLocation(ID, name.c_str()), x, y);
}

// ------------------------------------------------------------------------
void Shader::setVec3(const std::string& name, const glm::vec3& value) const
{
    glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

// ------------------------------------------------------------------------
void Shader::setVec3(const std::string& name, float x, float y, float z) const
{
    glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
}

// ------------------------------------------------------------------------
void Shader::setVec4(const std::string& name, const glm::vec4& value) const
{
    glUniform4fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

// ------------------------------------------------------------------------
void Shader::setVec4(
    const std::string& name, float x, float y, float z, float w) const
{
    glUniform4f(glGetUniformLocation(ID, name.c_str()), x, y, z, w);
}

// ------------------------------------------------------------------------
void Shader::setMat2(const std::string& name, const glm::mat2& mat) const
{
    glUniformMatrix2fv(
        glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

// ------------------------------------------------------------------------
void Shader::setMat3(const std::string& name, const glm::mat3& mat) const
{
    glUniformMatrix3fv(
        glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

// ------------------------------------------------------------------------
void Shader::setMat4(const std::string& name, const glm::mat4& mat) const
{
    glUniformMatrix4fv(
        glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//#include <stdio.h>
//#include <string>
//#include <vector>
//#include <iostream>
//#include <fstream>
//#include <algorithm>

// #include <stdlib.h>
//#include <string.h>

//#include <GL/glew.h>

//#include "shader.h"

// void Shader::loadGeneralShaders(const char *const vertex_file_path, const
// char *const fragment_file_path){
//    const auto vertexShaderID = compileShader(vertex_file_path,
//    GL_VERTEX_SHADER); const auto fragmentShaderID =
//    compileShader(fragment_file_path, GL_FRAGMENT_SHADER);

//    programID = glCreateProgram();
//    attachShader(vertexShaderID);
//    attachShader(fragmentShaderID);

//    destroy(vertexShaderID);
//    destroy(fragmentShaderID);
//}

// void Shader::loadComputeShader(const char* const compute_file_path){
//    const auto computeShaderID = compileShader(compute_file_path,
//    GL_COMPUTE_SHADER); programID = glCreateProgram();
//    attachShader(computeShaderID);

//    destroy(computeShaderID);

//}

// void Shader::use() const{
//    glUseProgram(programID);
//}

// GLuint Shader::compileShader(const char* const path, const GLuint& type)
// const{
//    GLuint shaderID = glCreateShader(type);
//    // read
//    std::string shaderCode;
//    std::ifstream shaderStream(path, std::ios::in);
//    if(shaderStream.is_open()){
//        std::string Line = "";
//        while(getline(shaderStream, Line)){
//            shaderCode += "\n" + Line;
//        }
//        shaderStream.close();
//    }else{
//        std::cout << "Impossible to open. Are you in the right directory? " +
//        std::string(path) << std::endl; getchar(); return GL_FALSE;
//    }
//    // compile
//    std::cout << "Compiling shader : " +  std::string(path) << std::endl;
//    const char* const sourcePointer = shaderCode.c_str();
//    glShaderSource(shaderID, 1, &sourcePointer , NULL);
//    glCompileShader(shaderID);

//    GLint Result = GL_FALSE;
//    int InfoLogLength;

//    // check shader
//    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
//    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
//    if ( InfoLogLength > 0 ){
//        std::vector<char> errorMessage(InfoLogLength+1);
//        glGetShaderInfoLog(shaderID, InfoLogLength, NULL, &errorMessage[0]);
//        std::cout << &errorMessage[0] << std::endl;
//    }
//    return shaderID;

//}

// void Shader::attachShader(const GLuint& shaderID) const{
//    glAttachShader(programID, shaderID);
//    glLinkProgram(programID);

//    GLint Result = GL_FALSE;
//    int InfoLogLength;

//    glGetProgramiv(programID, GL_LINK_STATUS, &Result);
//    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
//    if ( InfoLogLength > 0 ){
//        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
//        glGetProgramInfoLog(programID, InfoLogLength, NULL,
//        &ProgramErrorMessage[0]); std::cout << &ProgramErrorMessage[0] <<
//        std::endl;
//    }

//}

// void Shader::destroy(const GLuint& shaderID) const{
//    glDetachShader(programID, shaderID);
//    glDeleteShader(shaderID);
//}

// void Shader::setMatrix(const char* const name, const glm::mat4& value) const{
//    glUniformMatrix4fv(glGetUniformLocation(programID, name), 1, GL_FALSE ,
//    &value[0][0]);
//}

// void Shader::setFloat(const char *const name, const float &value) const{
//    glUniform1f(glGetUniformLocation(programID, name), value);
//}

// void Shader::setVec4(const char * const name, const glm::vec4 &value) const
//{
//    glUniform4fv(glGetUniformLocation(programID, name), 1, &value[0]);
//}
